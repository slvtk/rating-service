package com.example.rating.models;

public enum Role {
    USER, ADMIN
}
