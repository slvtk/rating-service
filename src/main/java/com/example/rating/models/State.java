package com.example.rating.models;

public enum State {
    NOT_CONFIRMED, CONFIRMED
}

