package com.example.rating.controllers;

import com.example.rating.dto.UserDto;
import com.example.rating.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


import java.util.List;

@RestController
public class RatingController {

    @Autowired
    private UsersService usersService;

    @GetMapping("/rating")
    public ResponseEntity<List<UserDto>> ratingUsers(){

        return ResponseEntity.ok(usersService.getAllUsersOrderByRatingDesc());

    }
}