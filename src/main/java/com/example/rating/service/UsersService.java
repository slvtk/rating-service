package com.example.rating.service;


import com.example.rating.dto.UserDto;
import com.example.rating.models.User;

import java.util.List;

public interface UsersService {

    List<UserDto> getAllUsers();

    List<UserDto> getAllUsersOrderByRatingDesc();

    User getAdmin();

}

