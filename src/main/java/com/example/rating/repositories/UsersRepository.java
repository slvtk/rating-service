package com.example.rating.repositories;


import com.example.rating.models.Role;
import com.example.rating.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UsersRepository extends JpaRepository<User, Long> {

    Optional<User> findByEmail(String email);

    Optional<User> findByConfirmCode(String confirmCode);

    Optional<User> findByRole(Role role);
}

